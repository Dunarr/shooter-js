import './style.scss'
import User from "../Models/User";
import Canvas from "./Canvas";
import {Keyboard, KeyboardTouche} from "../Models/Keyboard";
import {Direction, SupportedFactoryEntity} from "../Utils";
import Context from "../Models/Context";
import * as SocketIO from "socket.io";
import {ClickEvent, Mouse} from "../Models/Mouse";

const keyboard = Keyboard.Get();
export default class App {
    _canvas: Canvas;
    _io: SocketIO.Socket;
    _root: HTMLCanvasElement;
    _world: Context;

    constructor(selector: string, io: SocketIO.Socket) {
        this._io = io;
        this._world = new Context(10, this._io);
        this._root = <HTMLCanvasElement>document.getElementById(selector);
        this._canvas = new Canvas(this);
        const mouse = Mouse.Get(this);
        mouse.bind(this.fire.bind(this))
        this._world.bind(this._canvas);
        keyboard.on(KeyboardTouche.r, this.reload.bind(this))
        keyboard.on([KeyboardTouche.z, KeyboardTouche.upArrow], this.move(Direction.UP), this.stopMove(Direction.UP));
        keyboard.on([KeyboardTouche.d, KeyboardTouche.rightArrow], this.move(Direction.RIGHT), this.stopMove(Direction.RIGHT));
        keyboard.on([KeyboardTouche.s, KeyboardTouche.downArrow], this.move(Direction.DOWN), this.stopMove(Direction.DOWN));
        keyboard.on([KeyboardTouche.q, KeyboardTouche.leftArrow], this.move(Direction.LEFT), this.stopMove(Direction.LEFT));
    }

    reload() {
        this.player?.forceReload();
        this._io.emit("reload")
    }

    move(direction: Direction) {
        return () => {
            if (this.player) {
                this.player.move(direction, this._world);
                this._io.emit("move", direction)
            }
        }
    }

    stopMove(direction: Direction) {
        return () => {
            if (this.player)
                this.player.stopMove(direction, this._world);
            this._io.emit("stop", direction)
        }
    }

    fire(event: ClickEvent) {
        if(this.player)
            this.player.fire(event.x, event.y, this._world)
        this._io.emit("fire", event)
    }

    get player():User|null {
        const user = this._world.find(this._io.id)

        return (user && user.type === SupportedFactoryEntity.User) ? <User>user : null
    }
}
