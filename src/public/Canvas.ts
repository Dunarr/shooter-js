import {GRAPHICS, SCREEN_SIZE} from "../config";
import Entity from "../Models/Entity";
import ContextListener from "../Models/ContextListener";
import Context from "../Models/Context";
import App from "./App";
import User from "../Models/User";

export default class Canvas implements ContextListener {
    _app: App;
    _canvas: HTMLCanvasElement;
    _ctx: CanvasRenderingContext2D;
    _imageCache: ImageCache = {};

    constructor(app: App) {
        this._app = app;
        this._canvas = app._root;
        this._ctx = <CanvasRenderingContext2D>this._canvas.getContext("2d")
        this._canvas.width = SCREEN_SIZE.WIDTH;
        this._canvas.height = SCREEN_SIZE.HEIGHT;
    }

    public notify(context: Context) {
        this.render(context.entities)
    }

    public render(els: Entity[]) {

        this.clear()
        els.forEach(el => {
            this.drawEntity(el)
        })
        const player = this._app.player
        if (player) {
            this.drawUI(player)
        }
    }

    public drawUI(user: User): void {


        for (let i = 0; i < user.ammos; i++) {
            this.drawBullet(i)
        }
        for (let i = 0; i < user.life; i++) {
            this.drawHearth(i)
        }
        this._ctx.fillRect((SCREEN_SIZE.WIDTH - GRAPHICS.LOADBAR_WIDTH - GRAPHICS.MARGIN), SCREEN_SIZE.HEIGHT - (GRAPHICS.MARGIN + GRAPHICS.BULLET_HEIGHT + GRAPHICS.BULLET_MARGIN + GRAPHICS.SUBLOADBAR_HEIGHT), GRAPHICS.LOADBAR_WIDTH, GRAPHICS.SUBLOADBAR_HEIGHT)
        this._ctx.fillRect((SCREEN_SIZE.WIDTH - GRAPHICS.MARGIN) - (GRAPHICS.LOADBAR_WIDTH * user.loadBar), SCREEN_SIZE.HEIGHT - (GRAPHICS.MARGIN + GRAPHICS.BULLET_HEIGHT + GRAPHICS.BULLET_MARGIN + GRAPHICS.SUBLOADBAR_MARGIN + GRAPHICS.SUBLOADBAR_HEIGHT + GRAPHICS.LOADBAR_HEIGHT), (GRAPHICS.LOADBAR_WIDTH * user.loadBar), GRAPHICS.LOADBAR_HEIGHT)
    }

    drawBullet(number: number) {
        this._ctx.fillRect((SCREEN_SIZE.WIDTH - GRAPHICS.MARGIN) - ((GRAPHICS.BULLET_MARGIN + GRAPHICS.BULLET_WIDTH) * number + GRAPHICS.BULLET_WIDTH), SCREEN_SIZE.HEIGHT - (GRAPHICS.MARGIN + GRAPHICS.BULLET_HEIGHT), GRAPHICS.BULLET_WIDTH, GRAPHICS.BULLET_WIDTH)
    }
    drawHearth(number: number) {
        this.drawImage("heart.png", GRAPHICS.MARGIN + (GRAPHICS.HEART_WIDTH + GRAPHICS.HEART_MARGIN)* number, SCREEN_SIZE.HEIGHT - GRAPHICS.MARGIN - GRAPHICS.HEART_HEIGHT, GRAPHICS.HEART_WIDTH, GRAPHICS.HEART_HEIGHT)
    }

    public clear() {
        this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
    }

    drawImage(sprite:string, x:number, y: number, width: number, height: number) {
        this.loadImage(sprite).then(image => {
            this._ctx.drawImage(image, x, SCREEN_SIZE.HEIGHT - y, width, height)
        })
    }

    drawEntity(el: Entity) {
        const sprite = el.getSprite();
        const x = el.getX()
        const y = el.getY()
        const width = el.getWidth()
        const height = el.getHeight()
        this.drawImage(sprite, x, y, width, height)
    }

    public loadImage(path: string): Promise<HTMLImageElement> {
        return new Promise<HTMLImageElement>((resolve, reject) => {
            if (typeof this._imageCache[path] !== "undefined") {
                resolve(this._imageCache[path])
            } else {
                const image = document.createElement("img");
                image.onload = () => {
                    resolve(image)
                }
                image.onerror = () => {
                    reject(`image "${path}" not found`)
                }
                image.src = `/assets/sprites/${path}`;
                this._imageCache[path] = image
            }
        })

    }
}

interface ImageCache {
    [path: string]: HTMLImageElement
}
