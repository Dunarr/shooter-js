import App from "../public/App";
import {SCREEN_SIZE} from "../config";

export class Mouse {
    private _listeners: ((event:ClickEvent) => void)[] = []
    private static instance: Mouse|null = null;
    private _root: HTMLCanvasElement;
    private constructor(context: App) {
        this._root = context._root
        this._root.addEventListener("click", this.notify.bind(this))
    }
    notify(event: MouseEvent) {
        const x = (event.x) / this._root.offsetWidth * SCREEN_SIZE.WIDTH + this._root.getBoundingClientRect().left
        const y = SCREEN_SIZE.HEIGHT-(event.y) / this._root.offsetHeight * SCREEN_SIZE.HEIGHT + this._root.getBoundingClientRect().top
        this._listeners.forEach(listener => listener({x,y}))
    }
    public static Get(context: App) {
        if (!Mouse.instance) {
            Mouse.instance = new Mouse(context);
        }
        return Mouse.instance
    }
    public bind(callback: (event:ClickEvent)=>void){
        this._listeners.push(callback)
    }
}
export interface ClickEvent {
    x:number
    y:number
}
