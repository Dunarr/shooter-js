import Entity from "./Entity";
import {Socket} from "socket.io";
import User from "./User";
import {toArray, unserializeMap, UpdateData} from "../Utils";
import ContextListener from "./ContextListener";

export default class Context {
    private _time: number;
    private _entities: Entity[] = [];
    private _socket: Socket;
    private _isServer: boolean;
    private _listeners: ContextListener[] = [];

    constructor(frequency: number, socket: Socket, isServer: boolean = false) {
        this._time = new Date().getTime();
        this._socket = socket;
        this._isServer = isServer
        setInterval(this.update.bind(this), frequency)
        if(!this._isServer) {
            this._socket.on("update", this.loadUpdate.bind(this));
        }
    }

    public bind(listener: ContextListener): void {
        this._listeners.push(listener)
    }

    get entities(): Entity[] {
        return this._entities;
    }

    get time(): number {
        return this._time;
    }



    public update(): void {
        const delta = new Date().getTime() - this._time;
        this._time = new Date().getTime();
        this._entities.forEach(entity => {
            entity.update(delta, this)
        })
        if (this._isServer) {
            this._socket.emit("update", this.serialize())
        }
        this._listeners.forEach(listener => {
            listener.notify(this);
        })
    }

    public serialize() : UpdateData{
        return {
            _time:this._time,
            _entities:this._entities
        }
    }

    public loadUpdate(server: UpdateData) {
        this._entities = unserializeMap(server._entities)
        this._time = server._time
        this._listeners.forEach(listener => {
            listener.notify(this);
        })
    }

    public deleteEntity(entity: Entity): boolean {
        const index = this._entities.indexOf(entity);
        if(index >= 0) {
            this._entities.splice(index, 1)
        }
        return index >= 0
    }

    public addEntitiy(entity: Entity): void {
        this._entities.push(entity)
    }

    public find(id: string): Entity|null {
        const entity = this._entities.find(el => el.id === id)
        return entity?entity:null
    }
    public doAction(action :()=>void) {
        this.update()
        action()
        this.update()
    }
}
