import Entity from "./Entity";
import {reduceVector, SupportedFactoryEntity, uuidv4} from "../Utils";
import Context from "./Context";
import {SCREEN_SIZE} from "../config";
import Mobile from "./Mobile";
import User from "./User";

export default class Bullet extends Mobile implements Entity{
    id: string;
    type: SupportedFactoryEntity = SupportedFactoryEntity.Bullet;
    private _speed = 1000;
    private _x: number = 1;
    private _y: number = 1;
    private _owner?: User;
    private _vector: { x: number, y:number }= {x:0, y:0};


    get speed(): number {
        return this._speed;
    }

    protected get x(): number {
        return this._x;
    }

    protected set x(value: number) {
        this._x = value;
    }


    protected get y(): number {
        return this._y;
    }

    protected set y(value: number) {
        this._y = value;
    }

    constructor(x?: number, y?: number, vector?: { x: number; y: number }, owner?: User) {
        super();
        this._owner = owner;
        this.id = uuidv4();
        if(x) {
            this._x = x;
        }
        if(y) {
            this._y = y;
        }
        if(vector){
            this._vector = reduceVector(vector.x, vector.y);
        }
    }

    getHeight(): number {
        return 15;
    }

    getSprite(): string {
        return "bullet.png";
    }

    getWidth(): number {
        return 15;
    }

    getX(): number {
        return this._x;
    }

    getY(): number {
        return this._y;
    }

    update(delta: number, context: Context): void {
        this.updatePosition(delta)
        if(this._x < 0 || this._x > SCREEN_SIZE.WIDTH || this._y < 0 || this._y > SCREEN_SIZE.HEIGHT) {
            context.deleteEntity(this);
        }
        /*context.entities.forEach(entity => {
            if(entity.type === SupportedFactoryEntity.User) {
                if(this._owner !== entity){
                    if(this.x > entity.getX() && this.x > entity.getX() - entity.getWidth() && this.y > entity.getY() && this.y > entity.getY() - entity.getHeight()){
                        (<User>entity).shoot()
                        context.deleteEntity(this)
                    }
                }
            }
        })*/
    }


    get vector(): { x: number; y: number } {
        return this._vector;
    }
}
