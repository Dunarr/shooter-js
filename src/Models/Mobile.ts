import Vector from "./Vector";
import Context from "./Context";
import {Direction} from "../Utils";

export default abstract class Mobile {
    abstract readonly vector: Vector
    abstract readonly speed: number
    protected abstract x: number
    protected abstract y: number
    public updatePosition(delta: number) {
        let deltaPos = delta * this.speed / 1000;
        this.x+= this.vector.x*deltaPos
        this.y+= this.vector.y*deltaPos
    }
}
