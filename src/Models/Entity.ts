import Context from "./Context";
import {SupportedFactoryEntity} from "../Utils";

export default interface Entity {
     update(delta: number, context: Context):void;
     type: SupportedFactoryEntity;
     id: string;
     getSprite(): string;
     getX(): number;
     getY(): number;
     getWidth(): number;
     getHeight(): number;
}
