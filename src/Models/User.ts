import {Direction, Movements, SupportedFactoryEntity, uuidv4} from "../Utils";
import Entity from "./Entity";
import Context from "./Context";
import Bullet from "./Bullet";
import Mobile from "./Mobile";
import Vector from "./Vector";
import {SCREEN_SIZE} from "../config";

export default class User extends Mobile implements Entity {
    private _isReloading: boolean = false
    private _life: number = 5
    private _reloadingTimer: number = 0;
    private _speed = 300;
    private _x: number = 1;
    private _y: number = 1;
    private baseAmmos: number = 6;
    private _ammos: number = this.baseAmmos;
    private movements: Movements = {};
    private reloadTime = 1000
    public rotation: number = 0;

    constructor() {
        super();
        this._x = Math.floor(Math.random() * 1500);
        this._y = Math.floor(Math.random() * 1000);
        this.id = uuidv4()
    }

    get life(): number {
        return this._life;
    }

    set life(value: number) {
        this._life = value;
    }

    get ammos(): number {
        return this._ammos;
    }

    get loadBar(): number {
        return this._reloadingTimer / this.reloadTime
    }

    get speed(): number {
        return this._speed;
    }

    get vector(): Vector {
        let size = 1
        if ((this.movements[Direction.LEFT] || this.movements[Direction.RIGHT]) && (this.movements[Direction.UP] || this.movements[Direction.DOWN])) {
            size = 1 / Math.sqrt(2)
        }
        const vector = {x: 0, y: 0}
        if (this.movements[Direction.LEFT]) {
            vector.x -= size
        }
        if (this.movements[Direction.RIGHT]) {
            vector.x += size
        }
        if (this.movements[Direction.UP]) {
            vector.y += size
        }
        if (this.movements[Direction.DOWN]) {
            vector.y -= size
        }
        return vector
    }

    protected get x(): number {
        return this._x;
    }

    protected set x(value: number) {
        this._x = value;
    }

    protected get y(): number {
        return this._y;
    }

    protected set y(value: number) {
        this._y = value;
    }

    shoot() {
        this.life--
    }

    public forceReload() {
        this._isReloading = true
    }

    public reload(delta: number) {
        if (this._isReloading) {
            this._reloadingTimer += delta;
        }
        if (this._reloadingTimer >= this.reloadTime) {
            this._ammos = this.baseAmmos
            this._isReloading = false
            this._reloadingTimer = 0
        }
    }

    fire(x: number, y: number, context: Context) {
        context.doAction(() => {
            if (this.useAmmo()) {
                const bullet = new Bullet(this._x, this._y, {x: x - this._x, y: y - this._y}, this)
                context.addEntitiy(bullet)
            }
        })
    }

    useAmmo(): boolean {
        if (!this._isReloading) {
            if (this._ammos > 0) {
                this._ammos--
                return true
            }
            this._isReloading = true
        }
        return false
    }

    getPosition() {
        return {
            x: this._x,
            y: this._y
        }
    }

    public move(direction: Direction, context: Context) {
        context.doAction(() => {
            this.movements[direction] = true;
        })
    }

    public stopMove(direction: Direction, context: Context) {
        context.doAction(() => {
            this.movements[direction] = false;
        })
    }

    getHeight(): number {
        return 30;
    }

    getSprite(): string {
        return "char1.png";
    }

    getWidth(): number {
        return 30;
    }

    getX(): number {
        return this.getPosition().x;
    }

    getY(): number {
        return this.getPosition().y;
    }

    public id: string;
    type = SupportedFactoryEntity.User

    update(delta: number, context: Context): void {
        this.updatePosition(delta)
        this.reload(delta)
        if (this.x < -this.getWidth() / 2) {
            this.x = -this.getWidth() / 2
        }
        if (this.y < +this.getHeight() / 2) {
            this.y = +this.getHeight() / 2
        }
        if (this.x > SCREEN_SIZE.WIDTH - this.getWidth() / 2) {
            this.x = SCREEN_SIZE.WIDTH - this.getWidth() / 2
        }
        if (this.y > SCREEN_SIZE.HEIGHT + this.getHeight() / 2) {
            this.y = SCREEN_SIZE.HEIGHT + this.getHeight() / 2
        }
    }
}

