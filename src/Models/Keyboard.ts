export class Keyboard {
    private static instance: Keyboard;
    private downListeners: KeyboardListenerList = {};
    private upListeners: KeyboardListenerList = {};

    private constructor() {
        window.addEventListener('keydown', this.keyDownCallback.bind(this));
        window.addEventListener('keyup', this.keyUpCallback.bind(this))
    }

    public static Get() {
        if (!Keyboard.instance) {
            Keyboard.instance = new Keyboard();
        }
        return Keyboard.instance
    }

    keyDownCallback(event: KeyboardEvent): void {
        if (typeof this.downListeners[event.keyCode] !== "undefined") {
            this.downListeners[event.keyCode].forEach(callback => callback())
        }
    }

    keyUpCallback(event: KeyboardEvent): void {
        if (typeof this.upListeners[event.keyCode] !== "undefined") {
            this.upListeners[event.keyCode].forEach(callback => callback())
        }
    }
    on(key: KeyboardTouche, callbackDown?: (()=>void), callbackUp?: (()=>void)): void;
    on(key: KeyboardTouche[], callbackDown?: (()=>void), callbackUp?: (()=>void)): void;
    on(key: KeyboardTouche|KeyboardTouche[], callbackDown?: (()=>void), callbackUp?: (()=>void)): void{
        if( Array.isArray(key)){
            key.forEach(el => {
                this.on(el, callbackDown, callbackUp)
            })
        } else {
            if (callbackDown){
                if (typeof this.upListeners[key] === "undefined") {
                    this.downListeners[key] = []
                }
                this.downListeners[key].push(callbackDown)
            }
            if(callbackUp) {
                if (typeof this.upListeners[key] === "undefined") {
                    this.upListeners[key] = []
                }
                this.upListeners[key].push(callbackUp)
            }
        }

    }
}

export enum KeyboardTouche {
    break = 3,
    backspace = 8,
    tab = 9,
    clear = 12,
    enter = 13,
    shift = 16,
    ctrl = 17,
    alt = 18,
    pause = 19,
    capsLock = 20,
    escape = 27,
    space = 32,
    pageUp = 33,
    pageDown = 34,
    end = 35,
    home = 36,
    leftArrow = 37,
    upArrow = 38,
    rightArrow = 39,
    downArrow = 40,
    select = 41,
    print = 42,
    execute = 43,
    printScreen = 44,
    insert = 45,
    delete = 46,
    help = 47,
    main0 = 48,
    main1 = 49,
    main2 = 50,
    main3 = 51,
    main4 = 52,
    main5 = 53,
    main6 = 54,
    main7 = 55,
    main8 = 56,
    main9 = 57,
    colon = 58,
    semicolon = 59,
    lowerThan = 60,
    equals = 61,
    a = 65,
    b = 66,
    c = 67,
    d = 68,
    e = 69,
    f = 70,
    g = 71,
    h = 72,
    i = 73,
    j = 74,
    k = 75,
    l = 76,
    m = 77,
    n = 78,
    o = 79,
    p = 80,
    q = 81,
    r = 82,
    s = 83,
    t = 84,
    u = 85,
    v = 86,
    w = 87,
    x = 88,
    y = 89,
    z = 90,
    leftWindows = 91,
    rightWindows = 92,
    numpad0 = 96,
    numpad1 = 97,
    numpad2 = 98,
    numpad3 = 99,
    numpad4 = 100,
    numpad5 = 101,
    numpad6 = 102,
    numpad7 = 103,
    numpad8 = 104,
    numpad9 = 105,
    multiply = 106,
    add = 107,
    numpadPeriod = 108,
    subtract = 109,
    decimalPoint = 110,
    divide = 111,
}

interface KeyboardListenerList {
    [key: string]: (() => void)[]
}
