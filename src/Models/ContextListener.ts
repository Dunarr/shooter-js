import Context from "./Context";

export default interface ContextListener {
    notify(context: Context): void;
}
