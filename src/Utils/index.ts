import User from "../Models/User";
import Entity from "../Models/Entity";
import Bullet from "../Models/Bullet";

export interface UserList {
    [id: string]: User
}

export interface UpdateData {
    _time: number;
    _entities: Entity[];
}

export enum Direction {
    "UP" = "UP",
    "RIGHT" = "RIGHT",
    "DOWN" = "DOWN",
    "LEFT" = "LEFT"
}

export function reduceVector(x:number, y:number): { x: number, y: number } {
    const factor = 1 / Math.sqrt((x ** 2) + (y ** 2))
    return {
        x: x * factor,
        y: y * factor,
    }
}

export function oppositeDirection(direction: Direction): Direction {
    switch (direction) {
        case Direction.DOWN:
            return Direction.UP;
        case Direction.LEFT:
            return Direction.RIGHT;
        case Direction.UP:
            return Direction.DOWN;
        case Direction.RIGHT:
            return Direction.LEFT
    }
}

export interface Movements {
    [key: string]: boolean
}


export function unserialize(data: Entity): Entity {
    let el: Entity;
    switch (data.type) {
        case SupportedFactoryEntity.User:
            el = new User();
            break;
        case SupportedFactoryEntity.Bullet:
            el = new Bullet();
            break;
        default:
            throw new Error('unknown entity')
    }
    Object.assign(el, data);
    return el
}

export function unserializeMap(data: Entity[]): Entity[] {
    return data.map(el => unserialize(el))
}

export function toArray<T>(el: {[key:string]:T}):T[] {
    return Object.keys(el).map(key => el[key])
}

export enum SupportedFactoryEntity {
    User,
    Bullet,
}

export function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
