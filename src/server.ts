import User from './Models/User'
import * as SocketIO from 'socket.io';
import {join, resolve} from "path"
import express = require("express");
import {Direction, toArray, UserList} from "./Utils";
import Context from "./Models/Context";
import {ClickEvent} from "./Models/Mouse";

const app: express.Application = express();
const http = require('http').createServer(app);
const io = require("socket.io")(http);

const connections: SocketIO.Socket[] = [];

const world = new Context(100, io, true);

app.get('/', function (req, res) {
    res.sendFile(resolve(__dirname, 'public/index.html'));
});
app.get('/assets/*', function (req, res) {
    res.sendFile(join(resolve(__dirname, '../'), req.path));
});
app.get('/*', function (req, res) {
    res.sendFile(join(resolve(__dirname, 'public/'), req.path));
});


io.on('connection', function (socket: SocketIO.Socket) {
    console.log('a user connected');
    const user = new User();
    user.id = socket.id

    connections.push(socket);
    world.addEntitiy(user)
    socket.on('disconnect', function () {
        console.log('user disconnected');
        world.deleteEntity(user)
        connections.splice(connections.indexOf(socket), 1)
    });

    socket.on("move", (direction:Direction) => {
        user.move(direction, world)
    })
    socket.on("stop", (direction:Direction) => {
        user.stopMove(direction, world)
    })
    socket.on("fire", (event:ClickEvent) => {
        user.fire(event.x, event.y, world)
    })
    socket.on("reload", () => {
        user.forceReload()
    })
});

http.listen(3000, function () {
    console.log('listening on *:3000, open localhost:3000 to play');
});
