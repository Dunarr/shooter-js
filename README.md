# Shooter JS

## Install dependencies
`nmp install`

## Run dev
`nmp run watch-client` // build client

`nmp run serve` // run server

## Run prod
`nmp run build` // build client and server

`nmp run start` // run server
