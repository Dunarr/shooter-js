const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
module.exports = {

  entry: {
    index: './src/public/index.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/public/'),
    publicPath: '/',
  },
  resolve: {
    extensions: ['.ts', '.mjs', '.js', '.json']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/public/index.html',
      filename: 'index.html',
      publicPath: '/',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      }
    ],
  },
};
